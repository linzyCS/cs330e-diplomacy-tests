#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------


# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()


""" #pragma: no cover
% coverage run --branch TestDiplomacy.py > TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
.....
----------------------------------------------------------------------
Ran 5 tests in 0.000s

OK


% coverage report -m >> TestDiplomacy.out


% cat TestDiplomacy.out
.....
----------------------------------------------------------------------
Ran 5 tests in 0.000s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Army.py               17      0      4      0   100%
Diplomacy.py          50      0     30      0   100%
TestDiplomacy.py      31      0      0      0   100%
--------------------------------------------------------------
TOTAL                 98      0     34      0   100%
"""